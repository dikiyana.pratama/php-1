<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih PHP</title>
</head>
<body>
    <h1>Berlatih string PHP</h1>
    
    <?php
        echo "<h3>Soal No 1</h3>";
        $first_sentece = "Hello PHP!";
        echo "String        :" . $first_sentece. "<br>";
        echo "Panjang string : ". strlen($first_sentece) . "<br>";
        echo "Jumlah Kata     : ". str_word_count($first_sentece) . "<br><br>";

        $second_sentece = "I'm ready for the challenges";
        echo "String        :" . $second_sentece. "<br>";
        echo "Panjang string : ". strlen($second_sentece) . "<br>";
        echo "Jumlah Kata     : ". str_word_count($second_sentece) . "<br><br>";

        echo "<h3>Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "String        :" . $string2. "<br>";
        echo "Kata pertama  : " . substr($string2,0,1) . "<br>";
        echo "Kata kedua    : " . substr($string2,2,4) . "<br>";
        echo "Kata ketiga   : " . substr($string2,6,8) . "<br>";

        echo "<h3>Soal No 3</h3>";
        $string3 = "PHP is old but sexy";
        echo "String        :" . $string3. "<br>";
        echo "Ganti kalimat terakhir  : " . str_replace("sexy", "awesome", $string3) . "<br>";
        

 
    ?>
</body>
</html>